;;;; underworld.asd

(asdf:defsystem #:underworld
  :description "Yet another X window manager."
  :author "Sameer Rahmani <lxsameer@gnu.org>"
  :license  "GPLv2"
  :version "0.0.1"
  :serial t
  :depends-on (#:clx)
  :components ((:file "package")
               (:file "underworld")))
